def find_first_digit(input_string: str) -> int | None:
    """
    Read a string and returns a first digit if it exists

    Parameters:
    - input_str (str): input string

    Returns:
    - char (int): return first digit from the input string
    """
    for char in input_string:
        if char.isdigit():
            return int(char)
    return None  # Return None if no digit is found


def replace_str_num(input_str: str) -> str:
    """
    Read a string a returns new string with substrings replaced

    Parameters:
    - input_str (str): Input string.

    Returns:
    - new_str: each occurance of key substring in an input string is
    replaced with the value from the dict.

    """
    nums = {
        "one": "o1ne",
        "two": "t2wo",
        "three": "t3ree",
        "four": "f4our",
        "five": "f5ive",
        "six": "s6ix",
        "seven": "s7even",
        "eight": "e8ight",
        "nine": "n9ine"
        }
    for key, value in nums.items():
        new_str = input_str.replace(key, value)
    return new_str


def read_file_to_list(file_path: str) -> list:
    """
    Read a file and return its content as a list of lines.

    Parameters:
    - file_path (str): The path to the file.

    Returns:
    - list: A list containing the lines of the file.
    """
    try:
        with open(file_path, 'r') as file:
            content = file.readlines()
            # Remove newline characters from each line
            content = [line.strip() for line in content]
        return content
    except FileNotFoundError:
        print(f"Error: File not found - {file_path}")
        return []
    except Exception as e:
        print(f"Error: {e}")
        return []


if __name__ == "__main__":
    lines = read_file_to_list("day1/input.txt")

    # Part01
    result = []
    for line in lines:
        first_digit = find_first_digit(line)
        second_digit = find_first_digit(line[::-1])
        result.append(first_digit * 10 + second_digit)
    print("Part01 answer is: ", sum(result))

    # Part02
    result = []
    for line in lines:
        line = replace_str_num(line)
        first_digit = find_first_digit(line)
        second_digit = find_first_digit(line[::-1])
        result.append(first_digit * 10 + second_digit)
    print("Part02 answer is: ", sum(result))
