
## About Advent of Code

Advent of Code is a series of programming puzzles created by Eric Wastl. The challenges cover a variety of topics and are released daily on the [Advent of Code website](https://adventofcode.com/). Participants are encouraged to solve the puzzles using their preferred programming language.

## Solutions Overview

- **Day 1:** [Trebuchet](day1/)
- **Day 2:** [Cube Conundrum](day2/)
- **Day 3:** [Gear Ratios](day3/)
- **Day 4:** [Scratchcards](day4/)

Each day's solution is organized in its respective directory, containing the source code and any necessary input files.

## Directory Structure

- `Day 1/`
  - `solution.py`
  - `input.txt`
- `Day 2/`
  - `solution.py`
  - `input.txt`
- ...
- `Day 25/`
  - `solution.py`
  - `input.txt`

## Running the Solutions

To run a specific day's solution, navigate to the corresponding directory and execute the provided script. Make sure you have the necessary dependencies installed, and update the input files if needed.

Example (in Python):
```bash
cd day1
python solution.py