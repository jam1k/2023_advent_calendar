def read_file_to_list(file_path: str) -> list:
    """
    Read a file and return its content as a list of lines.

    Parameters:
    - file_path (str): The path to the file.

    Returns:
    - list: A list containing the lines of the file.
    """
    try:
        with open(file_path, 'r') as file:
            content = file.readlines()
            # Remove newline characters from each line
            content = [line.strip() for line in content]
        return content
    except FileNotFoundError:
        print(f"Error: File not found - {file_path}")
        return []
    except Exception as e:
        print(f"Error: {e}")
        return []


def return_sum_of_winning_cards(lines: list) -> int:
    points = []
    for line in lines:
        winning_numbers, on_hands = line.strip().split("|")
        winning_numbers = winning_numbers.strip().split(":")[1]
        winning_numbers = winning_numbers.strip().split(" ")
        on_hands = on_hands.strip().split(" ")
        on_hands = list(filter(('').__ne__, on_hands))
        res = 0
        for num in on_hands:
            if num in winning_numbers:
                res = 1
                on_hands.remove(num)
                break

        for num in on_hands:
            if num in winning_numbers:
                res *= 2
        points.append(res)
    return sum(points)


if __name__ == "__main__":
    lines = read_file_to_list("day4/input.txt")
    part01 = return_sum_of_winning_cards(lines)
    print("Part01 answer is:", part01)
