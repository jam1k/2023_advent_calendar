MAX_RED = 12
MAX_GREEN = 13
MAX_BLUE = 14


def read_file_to_list(file_path: str) -> list:
    """
    Read a file and return its content as a list of lines.

    Parameters:
    - file_path (str): The path to the file.

    Returns:
    - list: A list containing the lines of the file.
    """
    try:
        with open(file_path, 'r') as file:
            content = file.readlines()
            # Remove newline characters from each line
            content = [line.strip() for line in content]
        return content
    except FileNotFoundError:
        print(f"Error: File not found - {file_path}")
        return []
    except Exception as e:
        print(f"Error: {e}")
        return []


def transform_list_to_dict(input_list: list) -> dict:
    """
    Transforms the input list to the dictionary

    Parameters:
    - input_list (list): Content of the input file as a list

    Returns:
    - games (dict): Returns the output of each game in the following forma:
    game1: [output1, output2, output3]
    """
    games = {}
    for line in input_list:
        line = line.strip().split(":")
        line[1] = line[1].strip().split(";")
        games[line[0]] = line[1]

    return games


def split_elements_in_dict(input_dict: dict) -> dict:
    """
    Split each element of the lists in a dictionary.

    Parameters:
    - input_dict (dict): The input dictionary.

    Returns:
    - result_dict (dict): A new dictionary with each element split.
    """
    result_dict = {}

    for game, rounds in input_dict.items():
        split_rounds = []
        for element in rounds:
            cubes = {}
            for cube_info in element.strip().split(','):
                cube = cube_info.strip().split(" ")
                cubes[cube[1]] = int(cube[0])
            split_rounds.append(cubes)
        result_dict[game] = split_rounds

    return result_dict


def is_game_valid(rounds: dict) -> bool:
    """
    Check if a game meets the specified limit conditions.

    Parameters:
    - rounds (list[dict]): List of dictionaries representing rounds for a game.

    Returns:
    - bool: True if the game is valid, False otherwise.
    """
    return all(
        ("blue" not in round_data or round_data["blue"] <= MAX_BLUE) and
        ("red" not in round_data or round_data["red"] <= MAX_RED) and
        ("green" not in round_data or round_data["green"] <= MAX_GREEN)
        for round_data in rounds
    )


def calculate_sum_possible_games(input_dict: dict) -> int:
    """
    Calculate the sum of scores for games that meet the limit conditions.

    Parameters:
    - input_dict (dict): Dictionary containing game information.

    Returns:
    - int: Sum of scores for valid games.
    """
    valid_games = [int(key.split()[1])
                   for key, rounds in input_dict.items()
                   if is_game_valid(rounds)]

    return sum(valid_games)


def calculate_power(input_dict: dict) -> int:
    """
    Calculate the power of each game based on the minumum
    number of cubes needed for each game.

    Parameters:
    - input_dict (dict): Dictionary containing game information.

    Returns:
    - int: Sum of calculated powers for all games.
    """
    powers = []

    for key, rounds in input_dict.items():
        power_blue = max(round.get("blue", 0) for round in rounds)
        power_red = max(round.get("red", 0) for round in rounds)
        power_green = max(round.get("green", 0) for round in rounds)

        powers.append(power_blue * power_red * power_green)
    return sum(powers)


if __name__ == "__main__":
    lines = read_file_to_list("day2/input.txt")
    games = split_elements_in_dict(transform_list_to_dict(lines))
    print("Part01 answer is:", calculate_sum_possible_games(games))
    print("Part02 answer is:", calculate_power(games))
